<?php
declare(strict_types = 0);

namespace Eison\Utils\Helper;

/**
 * Class BcHelper
 *
 * @package Eison\Helper
 */
class BcHelper
{
    /**
     * Add two arbitrary precision numbers
     *
     * @param number $number1
     * @param number $number2
     * @param int    $scale
     * @return string
     */
    public static function add($number1, $number2, int $scale = 0)
    {
        return (string)\round(\bcadd($number1, $number2, \bcadd($scale, 2)), $scale);
    }

    /**
     * Subtract one arbitrary precision number from another
     *
     * @param number $number1
     * @param number $number2
     * @param number $scale
     */
    public static function sub($number1, $number2, $scale = 0)
    {
        return (string)\round(\bcsub($number1, $number2, \bcadd($scale, 2)), $scale);
    }

    /**
     * Multiply two arbitrary precision numbers
     *
     * @param number $number1
     * @param number $number2
     * @param number $scale
     */
    public static function mul($number1, $number2, $scale = 0)
    {
        return (string)\round(\bcmul($number1, $number2, \bcadd($scale, 2)), $scale);
    }

    /**
     * Divide two arbitrary precision numbers
     *
     * @param number $number1
     * @param number $number2
     * @param number $scale
     */
    public static function div($number1, $number2, $scale = 0)
    {
        return (string)\round(\bcdiv($number1, $number2, \bcadd($scale, 2)), $scale);
    }
}