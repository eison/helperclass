<?php
declare(strict_types = 0);

namespace Eison\Utils\Helper;

/**
 * Class ArrHelper
 *
 * @package Eison\Utils\Helper
 */
class ArrHelper
{
    /**
     * Sort a two-dimensional array by multiple columns
     *
     * @param array  $rowset  The target array
     * @param string $keyname Sort key
     * @param int    $sort    See https://php.net/manual/en/function.array-multisort.php
     * @return array
     */
    private static function sort_byMultiCols(array $rowset, string $keyname, int $sort): array
    {
        $sortColumn = [];

        foreach ($rowset as $i => $value) {
            $sortColumn[$i] = $value[$keyname];
        }

        // Sort multiple or multi-dimensional arrays.
        \array_multisort($sortColumn, $sort, $rowset);

        return $rowset;
    }

    /**
     * Merge one or more arrays
     *
     * @param array $originRow
     * @param array $combinedRow
     * @return array
     */
    private static function arrayMerge(array $originRow, array $combinedRow): array
    {
        foreach ($combinedRow as $key => $value) {
            if ((!isset($originRow[$key]) || !$originRow[$key]) // 自动判断组合双方的数据情况，用有数据的作为最终的值（没有设置值）
                || ($value || 0 === $value || '0' === $value)   // 有设置值 （0、"0" 被包含）
            ) {
                $originRow[$key] = $value;
            }
        }

        return $originRow;
    }

    /**
     * Sorts an array by the specified key
     *
     * @param array  $array   The target array
     * @param string $keyname Sort key
     * @param int    $dir     See https://php.net/manual/en/function.array-multisort.php
     * @return array
     */
    public static function sort(array $array, string $keyname, int $dir = SORT_ASC): array
    {
        return self::sort_byMultiCols($array, $keyname, $dir);
    }

    /**
     * Merges two two-dimensional arrays according to the specified column
     * Sql join on like
     *
     * @param array  $consequence
     * @param array  $combined
     * @param string $field
     * @param bool   $fill
     * @return array
     */
    public static function join(array $consequence, array $combined, string $field, bool $fill = false): array
    {
        $manipulation = array();

        foreach ($combined as $value) {
            $manipulation[$value[$field]] = $value;
        }

        if ($fill) {
            // Generates an array structure containing empty string defaults
            $stuct = \array_fill_keys(\array_keys(\end($combined)), '');

            foreach ($consequence as $i => $value) {
                // Set default value if null
                $combined = $manipulation[$value[$field]] ?? $stuct;
                $consequence[$i] = self::arrayMerge($value, $combined);
            }
        } else {
            foreach ($consequence as $i => $value) {
                $consequence[$i] = self::arrayMerge($manipulation[$value[$field]], $value);
            }
        }

        return $consequence;
    }

    /**
     * Group a two-dimensional array into multiple columns
     * Sql group by `column` like
     *
     * @param array  $manipulation
     * @param bool   $reverse
     * @param bool   $discard
     * @param string ...$colArray
     * @return array
     */
    public static function group(array $manipulation, bool $reverse = true, bool $discard = false, string ...$colArray)
    {
        $consequence = array();

        $manipulation = $reverse ? \array_reverse($manipulation) : $manipulation;
        $colArray = \array_slice(\func_get_args(), 3);

        foreach ($manipulation as $arr) {
            $keyname = '';

            foreach ($colArray as $value) {
                $keyname .= \sprintf('%s:', $arr[$value]);
            }

            if ($discard) {
                unset($arr[$value]);
            }

            $consequence[$keyname] = $arr;
        }

        return $consequence ? \array_values($consequence) : $consequence;
    }

    /**
     * Sum group by column
     *
     * @param mixed $sumCol
     * @param array $manipulation
     * @param array $consequence
     * @return mixed
     */
    public static function columnsSum($sumCol, array $manipulation, array $consequence = [])
    {
        $sumCols = is_string($sumCol) ? \explode(',', $sumCol) : $sumCol;

        foreach ($sumCols as $column) {
            $consequence[$column] = \array_sum(\array_column($manipulation, $column));
        }

        return \count($consequence) == 1 ? \end($consequence) : $consequence;
    }

    /**
     * Two arrays are merged and populated with default values
     *
     * @param array  $consequence
     * @param array  $combined
     * @param string $field
     * @return array
     */
    public static function leftJoin(array $consequence, array $combined, string $field): array
    {
        if (\count($combined) == \count($combined, 1)) {
            // Fills the array with the specified keys and values.
            $stuct = \array_fill_keys($combined, '');

            \array_walk($consequence, function (&$value) use ($stuct) {
                $value += $stuct;
            });
        } else {
            $consequence = self::join($consequence, $combined, $field, true);
        }

        return $consequence;
    }

    /**
     * Picks the data from an array and assigns a default value if it does not exist.
     *
     * @param array|string $field  Pick up columns
     * @param array        $sample Collection
     * @param mixed        $defVal Sets deafult value
     * @param array        $consequence
     * @return mixed
     */
    public static function getValue($field, array $sample, $defVal = null, array $consequence = [])
    {
        if ('*' !== $field) {
            $field = \is_string($field) ? \explode(',', $field) : $field;

            foreach ($field as $key) {
                if (\strpos($key, '.') !== false) {
                    list($field, $column) = \explode('.', $key);
                    $consequence[$column] = $sample[$field][$column] ?? $defVal;
                    
                    continue;
                }

                if (\strpos($key, '=') !== false) {
                    list($field, $alias) = \explode('=', $key);
                    $consequence[$alias] = $sample[$field] ?? $defVal;
                    
                    continue;
                }

                $consequence[$key] = $sample[$key] ?? $defVal;
            }
        }

        return \count($consequence) == 1 ? \end($consequence) : $consequence;
    }

    /**
     * Extracts the specified field values of the array as the array index
     *
     * @param string $column
     * @param array  $manipulation
     * @return array
     */
    public static function columnTokey(string $column, array $manipulation): array
    {
        $consequence = array();

        foreach ($manipulation as $value) {
            $consequence[$value[$column]] = $value;
        }

        return $consequence;
    }

    /**
     * Inserts a fragment of data into a position in an array，
     * Length changes but old data is not lost
     *
     * @param array $consequence
     * @param array $haystack
     * @param mixed $index
     */
    public static function splitJoint(array &$consequence, $haystack, $index = false): void
    {
        // Concatenation at the end when no insertion position is defined.
        $index = (false === $index) ? \count($consequence) : $index;

        // Remove a portion of the array and replace it with something else
        $extracted = \array_splice($consequence, $index, \count($haystack), $haystack);

        // Append
        \array_splice($consequence, \count($consequence), \count($extracted), $extracted);
    }

    /**
     * Pop the element off the end of array
     *
     * @param string|array $field
     * @param array        $sample
     * @return mixed
     */
    public static function pop($field, array &$sample)
    {
        $fields = \is_string($field) ? \explode(',', $field) : $field;
        $consequence = self::getValue($fields, $sample);

        foreach ($fields as $field) {
            unset($sample[$field]);
        }

        return $consequence;
    }

    /**
     * Removes an element from an array that contains a specific value
     *
     * @param array $sample
     * @param null  $placeholder
     * @return array
     */
    public static function rmValue(array &$sample, $placeholder = null): array
    {
        foreach ($sample as $key => $value) {
            if ($value === $placeholder) {
                unset($sample[$key]);
            }
        }

        return $sample;
    }
}