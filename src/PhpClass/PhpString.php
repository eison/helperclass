<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2019 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace Eison\Utils\PhpClass;

/**
 * Class PhpString
 *
 * @package     Eison\Utils\PhpClass
 * @description PHP string functions collection
 * @author      eison.icu@gmail.com
 * @date        2021-08-25 09:54:58 via Ubuntu
 */
class PhpString
{
    /**
     * Split a string by string
     *
     * @param      $delimiter
     * @param      $string
     * @param null $limit
     * @return false|string[]
     */
    public static function explode($delimiter, $string, $limit = null)
    {
        return \explode($delimiter, $string);
    }

    /**
     * Strip whitespace (or other characters) from the beginning and end of a string
     *
     * @param string $str
     * @param string $charlist
     * @return string
     */
    public static function trim(string $str, $charlist = " \t\n\r\0\x0B"): string
    {
        return \trim($str, $charlist);
    }

    /**
     * Strip whitespace (or other characters) from the beginning of a string
     *
     * @param string $str
     * @param string $charlist
     * @return string
     */
    public static function ltrim(string $str, $charlist = " \t\n\r\0\x0B"): string
    {
        return \ltrim($str, $charlist);
    }

    /**
     * Strip whitespace (or other characters) from the beginning of a string
     *
     * @param string $str
     * @param string $charlist
     * @return string
     */
    public static function rtrim(string $str, $charlist = " \t\n\r\0\x0B"): string
    {
        return \rtrim($str, $charlist);
    }

    /**
     * Return a formatted string
     *
     * @param string $format
     * @param mixed  ...$args
     * @return string
     */
    public static function sprintf(string $format, ...$args): string
    {
        return \sprintf($format, ...$args);
    }

    /**
     * Make a string uppercase
     *
     * @param string $string
     * @return string
     */
    public static function toUpper(string $string): string
    {
        return \strtoupper($string);
    }

    /**
     * Make a string lowercase
     *
     * @param string $string
     * @return string
     */
    public static function toLower(string $string): string
    {
        return \strtolower($string);
    }

    /**
     * Make a string's first character lowercase
     *
     * @param string $string
     * @return string
     */
    public static function firstToLower(string $string): string
    {
        return \lcfirst($string);
    }
}