<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2019 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace Eison\Utils\PhpClass;

/**
 * Class PhpArray
 *
 * @package     Eison\Utils\PhpClass
 * @description PHP array functions collection
 * @author      eison.icu@gmail.com
 * @date        2021-08-25 09:54:58 via Ubuntu
 */
class PhpArray
{
    /**
     * Return all the keys of an array
     *
     * @param array $input
     * @param mixed $search_value
     * @param null  $strict
     * @return array
     */
    public static function keys(array $input, $search_value = 'search_value', $strict = null): array
    {
        if ('search_value' !== $search_value) {
            return \array_keys($input, $search_value, $strict);
        }

        return \array_keys($input);
    }

    /**
     * Return all the values of an array
     *
     * @param array $input
     * @return array
     */
    public static function values(array $input): array
    {
        return \array_values($input);
    }

    /**
     * Pop the element off the end of array
     *
     * @param array $array
     * @return mixed
     */
    public static function pop(array &$array)
    {
        return \array_pop($array);
    }

    /**
     * Push elements onto the end of array
     *
     * @param array $array
     * @param mixed ...$vars
     * @return int
     */
    public static function push(array &$array, ...$vars): int
    {
        return \array_push($array, ...$vars);
    }

    /**
     * Merge one or more arrays
     *
     * @param array ...$arrays
     * @return array
     */
    public static function merge(array ...$arrays): array
    {
        return \array_merge(...$arrays);
    }

    /**
     * Apply a user function to every member of an array
     *
     * @param array $array
     * @param mixed $funcname
     * @param null  $userdata
     * @return bool
     */
    public static function walk(array &$array, $funcname, $userdata = null): bool
    {
        return \array_walk($array, $funcname, $userdata);
    }

    /**
     * Checks if a value exists in an array
     *
     * @param mixed $needle
     * @param array $haystack
     * @param bool  $strict
     * @return bool
     */
    public static function in($needle, array $haystack, $strict = false): bool
    {
        return \in_array($needle, $haystack, $strict);
    }

    /**
     * Return the values from a single column in the input array
     *
     * @param mixed $column
     * @param array $array
     * @param null  $index_key
     * @return array
     */
    public static function column($column, array $array, $index_key = null): array
    {
        return \array_column($array, $column, $index_key);
    }

    /**
     * Join array elements with a string
     *
     * @param string $glue
     * @param array  $pieces
     * @return string
     */
    public static function implode($glue = "", array $pieces): string
    {
        return \implode($glue, $pieces);
    }

    /**
     * Counts all elements in an array, or something in an object.
     *
     * @param array|null $var
     * @param int        $mode
     * @return int
     */
    public static function count($var, $mode = COUNT_NORMAL): int
    {
        return \count($var, $mode);
    }

    /**
     * Calculate the sum of values in an array
     *
     * @param array $array
     * @return float|int
     */
    public static function sum(array $array)
    {
        return \array_sum($array);
    }

    /**
     * Removes duplicate values from an array
     *
     * @param array $array
     * @param int   $sort_flags
     * @return array
     */
    public static function unique(array $array, $sort_flags = SORT_STRING): array
    {
        return \array_unique($array, $sort_flags);
    }

    /**
     * Computes the difference of arrays
     *
     * @param array $array1
     * @param array $array2
     * @param array $_
     * @return array
     */
    public static function diff(array $array1, array $array2, array $_ = []): array
    {
        return \array_diff($array1, $array2, $_);
    }

    /**
     * Return an array with elements in reverse order
     *
     * @param array $array
     * @param null  $preserve_keys
     * @return array
     */
    public static function reverse(array $array, $preserve_keys = null): array
    {
        return \array_reverse($array, $preserve_keys);
    }

    /**
     * Fill an array with values, specifying keys
     *
     * @param array $keys
     * @param mixed $value
     * @return array
     */
    public static function fillKey(array $keys, $value): array
    {
        return \array_fill_keys($keys, $value);
    }

    /**
     * Creates an array by using one array for keys and another for its values
     *
     * @param array $keys
     * @param array $value
     * @return array
     */
    public static function combine(array $keys, array $value): array
    {
        return \array_combine($keys, $value);
    }

    /**
     * Sort an array
     *
     * @param array $array
     * @param int   $sort_flags
     * @return array
     */
    public static function sort(array $array, int $sort_flags = SORT_ASC): array
    {
        \sort($array, $sort_flags);
        return $array;
    }

    /**
     * Sort an array by key
     *
     * @param array $array
     * @param int   $sort_flags
     * @return array
     */
    public static function ksort(array $array, int $sort_flags = SORT_REGULAR): array
    {
        \ksort($array, $sort_flags);
        return $array;
    }

    /**
     * Sort an array by key in reverse order
     *
     * @param array $array
     * @param int   $sort_flags
     * @return array
     */
    public static function krsort(array $array, int $sort_flags = SORT_REGULAR): array
    {
        \krsort($array, $sort_flags);
        return $array;
    }
}