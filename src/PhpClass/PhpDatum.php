<?php
declare(strict_types = 1);

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2019 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace Eison\Utils\PhpClass;

/**
 * Class PhpDatum
 *
 * @package     Eison\Utils\PhpClass
 * @description Data types and structures
 * @author      eison.icu@gmail.com
 * @date        2021-08-25 16:16:55 via Ubuntu
 */
class PhpDatum
{
    /**
     * Destroys the specified variables.
     *
     * @param $var
     */
    public static function unset($var)
    {
        unset($var);
    }

    /**
     * Get the type of a variable
     *
     * @param $var
     * @return string
     */
    public static function getType($var): string
    {
        return \gettype($var);
    }

    /**
     * Finds out whether a variable is a boolean
     *
     * @param $var
     * @return bool
     */
    public static function isBool($var): bool
    {
        return \is_bool($var);
    }

    /**
     * Finds whether a variable is an array
     *
     * @param $var
     * @return bool
     */
    public static function isArray($var): bool
    {
        return \is_array($var);
    }

    /**
     * Find whether the type of a variable is string
     *
     * @param $var
     * @return bool
     */
    public static function isString($var): bool
    {
        return \is_string($var);
    }

    /**
     * Finds whether a variable is an object
     *
     * @param $var
     * @return bool
     */
    public static function isObject($var): bool
    {
        return \is_object($var);
    }

    /**
     * Finds whether a variable is &null;
     *
     * @param $var
     * @return bool
     */
    public static function isNull($var): bool
    {
        return (null === $var);
    }

    /**
     * Get the integer value of a variable
     *
     * @param mixed $var
     * @param int   $base
     * @return int
     */
    public static function toInt($var, $base = 10): int
    {
        return \intval($var, $base);
    }

    /**
     * Get the array value of a variable
     *
     * @param mixed $var
     * @return array
     */
    public static function toArray($var): array
    {
        return (array)$var;
    }
}